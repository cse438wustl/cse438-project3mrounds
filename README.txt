In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
* Why did you choose this feature?
* How did you implement it?

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
	-10 Missing
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
	-5 Missing
(5 points) Double tapping allows the Player to stand
	-5 Missing
(10 points) Cards being dealt are animated, and all cards are visible.
	-7 No animations
(5 points) If the Player goes over 21, they automatically bust
	-3 The game logic seemed off to me, sometimes it would tell me I busted when I was well below 21. Sometimes it would let me keep going even though I had more than 21. I honestly could not figure out how to win a game.
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
	-5 I was not able to find any code that sent game data to firebase
(5 points) A new round is automatically started after each round
(5 points) Player can logout
	-5 Missing
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!
	-15 Missing

Total: 55 / 110
