package com.example.cse438.blackjack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.support.constraint.ConstraintLayout
import android.support.v4.widget.ImageViewCompat
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.ImageView
import android.widget.Button
import android.widget.RelativeLayout
import com.example.cse438.blackjack.util.CardRandomizer
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlin.random.Random
class AccountActivity : AppCompatActivity() {
    private var fAuth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.account_layout)
        fAuth = FirebaseAuth.getInstance()
        val message:TextView = findViewById(R.id.userName)
        val logout:Button = findViewById(R.id.logout)
        val leaderButton:Button = findViewById(R.id.leaderBoard)
        val hit:Button = findViewById(R.id.hit)
        val stand:Button = findViewById(R.id.stand)
        var score1:Int = 0
        var score2:Int = 0
        message.setText("Welcome back, " + fAuth.getCurrentUser()!!.getEmail()!!.toString())
        var ints: ArrayList<Int> = CardRandomizer().getIDs(this)
        var img1:ImageView = findViewById(R.id.card1)
        var img2:ImageView = findViewById(R.id.card2)
        var img3:ImageView = findViewById(R.id.card3)
        var img4:ImageView = findViewById(R.id.card4)
        var int1:Int = ints[Random.nextInt(ints.size)]
        var int2:Int = ints[Random.nextInt(ints.size)]
        var int3:Int = ints[Random.nextInt(ints.size)]
        var int4:Int = ints[Random.nextInt(ints.size)]
        img1.setImageResource(int1)
        img2.setImageResource(int2)
        img3.setImageResource(int3)
        score1+=(int1-2131165277)%13
        score1+=(int2-2131165277)%13
        score2+=(int3-2131165277)%13
        score2+=(int4-2131165277)%13
        Log.e("score", ""+score1)
        img4.setImageResource(R.drawable.back)
        ints.remove(int1)
        ints.remove(int2)
        ints.remove(int3)
        ints.remove(int4)
        logout.setOnClickListener(View.OnClickListener
        {
            fAuth.signOut()
            startActivity(Intent(this@AccountActivity, FireAuth::class.java))

        })
        leaderButton.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@AccountActivity, LeaderBoard::class.java))

        })
        hit.setOnClickListener(View.OnClickListener {
            var newImg:ImageView = ImageView(this)
            var newInt:Int = ints[Random.nextInt(ints.size)]
            newImg.setImageResource(newInt)

            ints.remove(newInt)
            val layout:RelativeLayout = findViewById(R.id.r1)
            var param:RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT
            )
            layout.addView(newImg,param)
            score1+=(newInt-2131165277)%13
            if(score1>21)
            {
                message.setText("Bust, Cpu wins")
                finish()
                startActivity(getIntent())
            }
        })
        stand.setOnClickListener(View.OnClickListener {

            img4.setImageResource(int4)
            ints.remove(int4)
            while(score2<17)
            {
                var newImg:ImageView = ImageView(this)
                var newInt:Int = ints[Random.nextInt(ints.size)]
                newImg.setImageResource(newInt)

                ints.remove(newInt)
                val layout:RelativeLayout = findViewById(R.id.r1)
                var param:RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
                layout.addView(newImg,param)
                score2+=(newInt-2131165277)%13
            }

            if(score2>21)
            {
                message.setText("Bust, player wins")
                finish()
                startActivity(getIntent())
            }
        })

    }
}
